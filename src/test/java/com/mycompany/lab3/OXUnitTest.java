/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class OXUnitTest {
    
    public OXUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWin_O_horizontall_output_true(){
        String[][]table = {{"O","O","O"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
 
    @Test
    public void testCheckWin_O_horizontal2_output_true(){
        String[][]table = {{"-","-","-"},{"O","O","O"},{"-","-","-"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_O_horizontal3_output_true(){
        String[][]table = {{"-","-","-"},{"-","-","-"},{"O","O","O"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_O_Verticecol1_output_true(){
        String[][]table = {{"O","-","-"},{"O","-","-"},{"O","-","-"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_O_Verticecol2_output_true(){
        String[][]table = {{"-","O","-"},{"-","O","-"},{"-","O","-"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_O_Verticecol3_output_true(){
        String[][]table = {{"-","-","O"},{"-","-","O"},{"-","-","O"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckTayeng1_output_true(){
        String[][]table = {{"O","-","-"},{"-","O","-"},{"-","-","O"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckTayeng2_output_true(){
        String[][]table = {{"-","-","O"},{"-","O","-"},{"O","-","-"}};
        String currentPlayer = "O";
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_X_horizontall_output_true(){
        String[][]table = {{"X","X","X"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_X_horizontal2_output_true(){
        String[][]table = {{"-","-","-"},{"X","X","X"},{"-","-","-"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_X_horizontal3_output_true(){
        String[][]table = {{"-","-","-"},{"-","-","-"},{"X","X","X"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_X_Verticecol1_output_true(){
        String[][]table = {{"X","-","-"},{"X","-","-"},{"X","-","-"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_X_Verticecol2_output_true(){
        String[][]table = {{"-","X","-"},{"-","X","-"},{"-","X","-"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_X_Verticecol3_output_true(){
        String[][]table = {{"-","-","X"},{"-","-","X"},{"-","-","X"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckXTayeng1_output_true(){
        String[][]table = {{"X","-","-"},{"-","X","-"},{"-","-","X"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckXTayeng2_output_true(){
        String[][]table = {{"-","-","X"},{"-","X","-"},{"X","-","-"}};
        String currentPlayer = "X";
        boolean result = Lab3.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckDraw_output_true(){
        int count = 9;
        boolean result = Lab3.checkDraw(count);
        assertEquals(true,result);
    } 
    
    
    
    
    
}
